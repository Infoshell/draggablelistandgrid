package ru.infoshell.draggablelistandgrid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.HashSet;
import java.util.Set;

import ru.infoshell.draggablelistandgrid.adapters.BaseDraggableListAdapter;
import ru.infoshell.draggablelistandgrid.utils.Lo;

@SuppressWarnings("ALL")
public class DraggableListView extends ListView {

    private static final int ANIM_TAG = R.id.header_anim_tag;
    private static final String ANIM_UP = "anim_up";
    private static final String ANIM_DOWN = "anim_down";

	private boolean isDragNow, isDragStarted;
    private long dragStartTime;
    private static final int COLLAPSE_TIME = 1100;
    private WindowManager windowManager;
    private int startPosition = INVALID_POSITION;
    private int dragPointOffset;
    private ImageView dragImageView;
    private int itemH;
    private int lastChildNum, startDraggedNum;
    private OnDragAndDropListener mDragNDropListener;
    private BaseDraggableListAdapter draggableListAdapter;
    private Set<View> animatedViews;



	public DraggableListView(Context context) {
		super(context);
		init();
	}



	public DraggableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}



	public DraggableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}



    private void init() {
        windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        animatedViews = new HashSet<View>();
    }



    private Animation getUpOrDownAnimation(int height, boolean isBack){
        Animation animation;
        if (isBack)
            animation = new TranslateAnimation(0, 0, height, 0);
        else
            animation = new TranslateAnimation(0 , 0 , 0 , height);
        animation.setDuration(100);
        if (!isBack) animation.setFillAfter(true);
        return animation;
    }



    public void setOnItemDragNDropListener(OnDragAndDropListener listener) {
		mDragNDropListener = listener;
	}



	public void setDragAdapter(BaseDraggableListAdapter adapter) {
		setAdapter(adapter);
	}






	public boolean isDragging() {
		return isDragNow;
	}

    public void setDragMode(boolean isDragNow){
        Lo.i("setDragMode "+isDragNow);
        this.isDragNow = isDragNow;
        if (isDragNow) dragStartTime = System.currentTimeMillis();
    }



    @Override
    public boolean onInterceptTouchEvent (MotionEvent ev){
        if (draggableListAdapter==null) draggableListAdapter = (BaseDraggableListAdapter)getAdapter();
        if (draggableListAdapter.isGridDragNow()){
            if ( draggableListAdapter.getDraggableGridYY()[0]<ev.getY() && draggableListAdapter.getDraggableGridYY()[1] > ev.getY()) {
                return false;
            }
            else {
                draggableListAdapter.cancelAllEditModes();
                return true;
            }
        }
        if (isDragNow) return true;
        return super.onInterceptTouchEvent(ev);
    }



    @Override
	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		final int x = (int)ev.getX();
		final int y = (int)ev.getY();

        if (!isDragNow || !isDraggingEnabled) return super.onTouchEvent(ev);

		switch (action) {
			case MotionEvent.ACTION_DOWN:
				startPosition = pointToPosition(x, y);
				
				if (startPosition != INVALID_POSITION) {
					int childPosition = startPosition - getFirstVisiblePosition();
                    if (childPosition>=0 && getDraggableListAdapter().isPositionDraggable(startPosition)) {
                        if (getChildAt(childPosition) == null) throw new IllegalArgumentException(
                                "(ACTION_DOWN) getChildAt " + childPosition + " is null   getChildCount: " + getChildCount());
                        itemH = getChildAt(childPosition).getHeight();
                        dragPointOffset = y - getChildAt(childPosition).getTop();
                        dragPointOffset -= ((int) ev.getRawY()) - y;

                        startDrag(childPosition, y);
                        drag(0, y);
                    }
				}
				
				break;
			case MotionEvent.ACTION_MOVE:
                Lo.i("ACTION_MOVE");

                if (!isDragStarted ) {
                    if (dragStartTime+COLLAPSE_TIME<System.currentTimeMillis()) {
                        Lo.w("ACTION_MOVE !");
                        startPosition = pointToPosition(x, y);
                        int childPosition = startPosition - getFirstVisiblePosition();
                        if (childPosition >= 0 && getDraggableListAdapter().isPositionDraggable(startPosition)) {
                            if (getChildAt(childPosition) == null) throw new IllegalArgumentException(
                                    "(ACTION_MOVE) getChildAt " + childPosition + " is null   getChildCount: " + getChildCount());
                            itemH = getChildAt(childPosition).getHeight();
                            dragPointOffset = y - getChildAt(childPosition).getTop();
                            dragPointOffset -= ((int) ev.getRawY()) - y;
                            startDrag(childPosition, y);
                        }
                    }
                    else return true;
                }
            //    else return true;

                // проверка что находится под пальцем при перетаскивании, если новая позиция то анимируем ее переезд
                int start = pointToPosition(x, y);
                int childNum = start - getFirstVisiblePosition();
                if (childNum<0 ) childNum = startDraggedNum; //?
                if (childNum!=lastChildNum && getDraggableListAdapter().isPositionDraggable(start)) {

                    int chlidNumDiff = lastChildNum - childNum;
                    if (chlidNumDiff>1 || chlidNumDiff<-1) Lo.e("chlidNumDiff "+chlidNumDiff);

                    // на случай рывка по Y, когда перескок пальца прошел больше, чем на 1 позицию надо обрабатывать пропущенные
                    int workChildNum = lastChildNum;
                    while (workChildNum!=childNum) {

                        if (workChildNum>childNum) workChildNum--;
                        else if (workChildNum<childNum)workChildNum ++;

                        View view = getChildAt(workChildNum);
                        if (view != null) {

                            if (workChildNum == startDraggedNum) {
                                View viewUp = getChildAt(workChildNum - 1);
                                if (viewUp != null && viewUp.getAnimation() != null) {
                                    viewUp.startAnimation(getUpOrDownAnimation(itemH, true));
                                    viewUp.setTag(ANIM_TAG, null);
                                } else {
                                    View viewDown = getChildAt(workChildNum + 1);
                                    if (viewDown != null && viewDown.getAnimation() != null) {
                                        viewDown.startAnimation(getUpOrDownAnimation(-itemH, true));
                                        viewDown.setTag(ANIM_TAG, null);
                                    }
                                }
                            }

                            //TODO для перескока анмации через игнорируемые нужно доделать
                            else if (workChildNum > lastChildNum && workChildNum != startDraggedNum) {

                                if (view.getTag(ANIM_TAG) != null && workChildNum - 1 != startDraggedNum) {
                                    View view2 = getChildAt(workChildNum - 1);
                                    if (view2 != null) {
                                        view2.startAnimation(getUpOrDownAnimation(itemH, true));
                                        view2.setTag(ANIM_TAG, null);
                                    }
                                } else {
                                    view.startAnimation(getUpOrDownAnimation(-itemH, false));
                                    view.setTag(ANIM_TAG, ANIM_UP);
                                }
                                animatedViews.add(view);

                            } else if (workChildNum < lastChildNum && workChildNum != startDraggedNum) {

                                if (view.getTag(ANIM_TAG) != null && workChildNum + 1 != startDraggedNum) {
                                    View view2 = getChildAt(workChildNum + 1);
                                    if (view2 != null) {
                                        view2.startAnimation(getUpOrDownAnimation(-itemH, true));
                                        view2.setTag(ANIM_TAG, null);
                                    }
                                } else {
                                    view.startAnimation(getUpOrDownAnimation(itemH, false));
                                    view.setTag(ANIM_TAG, ANIM_DOWN);
                                }
                                animatedViews.add(view);
                            }
                        }

                    }
                    lastChildNum = childNum;
                }
            //    Lo.i("childNum "+childNum + "  / " +lastChildNum+ " / " + startDraggedNum);

				drag(0, y);
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
			default:

				if (startPosition != INVALID_POSITION) {
					// check if the position is a header/footer
					int actualPosition =  pointToPosition(x,y);
					if (actualPosition > (getCount() - getFooterViewsCount()) - 1)
						actualPosition = INVALID_POSITION;

					stopDrag(startPosition - getFirstVisiblePosition(), actualPosition);
				}
				break;
		}
		
		return true;
	}



	private void startDrag(int childIndex, int y) {
		View item = getChildAt(childIndex);
        startDraggedNum = childIndex;
        lastChildNum = childIndex;
        Lo.i("startDrag childIndex: "+childIndex+ "  y: "+y+ "    item: "+item);
		
		if (item == null) return;

		long id = getItemIdAtPosition(startPosition);

		if (mDragNDropListener != null)
        	mDragNDropListener.onItemDrag(this, item, startPosition, id);

        BaseDraggableListAdapter dndAdapter = getDraggableListAdapter();

        dndAdapter.onItemDrag(this, item, startPosition, id);

		item.setDrawingCacheEnabled(true);

        Bitmap bitmap = Bitmap.createBitmap(item.getDrawingCache());
        
        WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
        mWindowParams.gravity = Gravity.TOP;
        mWindowParams.x = 0;
        mWindowParams.y = y - dragPointOffset;

        mWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        mWindowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        mWindowParams.format = PixelFormat.TRANSLUCENT;
        mWindowParams.windowAnimations = 0;
        
        Context context = getContext();
        ImageView v = new ImageView(context);
        v.setImageBitmap(bitmap);

        windowManager.addView(v, mWindowParams);
        dragImageView = v;
        
        item.setVisibility(View.INVISIBLE);
        item.invalidate();
        isDragStarted = true;
	}



	private void stopDrag(int childIndex, int endPosition) {
        Lo.i("stopDrag childIndex: "+childIndex +"   endPosition: "+endPosition  );
		if (dragImageView == null) return;

        for (View animView : animatedViews) {
            animView.clearAnimation();
            animView.setTag(ANIM_TAG, null);
        }
        animatedViews.clear();

		View item = getChildAt(childIndex);

        if (endPosition != INVALID_POSITION || childIndex>=0) {
            long id = getItemIdAtPosition(startPosition);

            if (mDragNDropListener != null)
                mDragNDropListener.onItemDrop(this, item, startPosition, endPosition, id);

            BaseDraggableListAdapter dndAdapter = getDraggableListAdapter();

            dndAdapter.onItemDrop(this, item, startPosition, endPosition, id);
        }
		
        dragImageView.setVisibility(GONE);
        windowManager.removeView(dragImageView);
        
        dragImageView.setImageDrawable(null);
        dragImageView = null;
        
        item.setDrawingCacheEnabled(false);
        item.destroyDrawingCache();
        
        item.setVisibility(View.VISIBLE);
        
        startPosition = INVALID_POSITION;
        
        invalidateViews();
        isDragStarted = false;
	}



	private void drag(int x, int y) {
		if (dragImageView == null) return;

		WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) dragImageView.getLayoutParams();
		layoutParams.x = x;
		layoutParams.y = y - dragPointOffset;

		windowManager.updateViewLayout(dragImageView, layoutParams);
	}

    private boolean isDraggingEnabled = true;

    public void setDraggingEnabled(boolean draggingEnabled) {
        this.isDraggingEnabled = draggingEnabled;
    }


    private BaseDraggableListAdapter getDraggableListAdapter() {
        return (BaseDraggableListAdapter) getAdapter();
    }


    public static interface OnDragAndDropListener {

        public void onItemDrag(DraggableListView parent, View view, int position, long id);

        public void onItemDrop(DraggableListView parent, View view, int startPosition, int endPosition, long id);
    }

}
