package ru.infoshell.draggablelistandgrid.adapters;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.nineoldandroids.view.ViewHelper;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ru.infoshell.draggablelistandgrid.DraggableListItem;
import ru.infoshell.draggablelistandgrid.DraggableListView;
import ru.infoshell.draggablelistandgrid.FooterClickListener;
import ru.infoshell.draggablelistandgrid.HeaderClickListener;
import ru.infoshell.draggablelistandgrid.R;
import ru.infoshell.draggablelistandgrid.grid.DraggableGridAdapter;
import ru.infoshell.draggablelistandgrid.grid.DraggableGridView;
import ru.infoshell.draggablelistandgrid.utils.ExpanderCollapser;
import ru.infoshell.draggablelistandgrid.utils.Lo;

/**
 * Адаптер для листа с гридами, с возможностью перетаскиваний.
 * <br> Для использования необходимо наследоваться от этого класса и переопределить методы:
 * <br> - initHeaderView(...)
 * <br> - initFooterView(...)
 * <br> - getViewForGrid(...)
 * <br> В наследуемом классе можно следовать паттерну ViewHolder для оптимизации прокрутки
 * <p/>
 * <br> Внимание! Т.к. внутри элемента листа грид развернут на всю высоту, в этом варианте реализации
 * возможны притормаживания на промотке при большом числе элементов грида
 * <p/>
 * <br><br>  Created by Andrey Riyk
 */
public abstract class BaseDraggableListAdapter<T extends DraggableListItem> extends BaseAdapter
        implements ListAdapter, AdapterView.OnItemClickListener, DraggableListView.OnDragAndDropListener, DraggableGridView.OnDropListener, DraggableGridView.OnDragListener {

    private final Context context;
    private DraggableListView draggableListView;
    private Set<ViewHolderBase> holdersSet;
    private Set<DraggableGridView> gridsInEditMode;
    private Set<Integer> noDraggableHeaders;
    private boolean isAllCollapsed;
    private int gridItemHeight, gridItemHeightSpacing;
    private boolean needHeader, needFooter;
    private boolean allowDragGrid = true;
    private FooterClickListener footerClickListener;
    private HeaderClickListener headerClickListener;
    private DraggableGridView.OnDragListener onDragListener;
    private DraggableGridView.OnDropListener onDropListener;
    private OnGridItemClickListener onGridItemClickListener;

    private boolean isGridDragNow;
    private int[] draggableGridYY = new int[2];
    private final int columnsCnt;

    private boolean listWobbleAnimation = true;
    private List<ObjectAnimator> wobbleAnimators = new LinkedList<ObjectAnimator>();
    private List<com.nineoldandroids.animation.ObjectAnimator> wobbleAnimatorsPreHoneycomb = new LinkedList<com.nineoldandroids.animation.ObjectAnimator>();


    public BaseDraggableListAdapter(Context context, DraggableListView draggableListView, int columnsCnt, boolean needHeader, boolean needFooter) {
        this.context = context;
        this.draggableListView = draggableListView;
        holdersSet = new HashSet<ViewHolderBase>();
        noDraggableHeaders = new HashSet<Integer>();
        gridsInEditMode = new HashSet<DraggableGridView>();
        this.columnsCnt = columnsCnt;
        this.needHeader = needHeader;
        this.needFooter = needFooter;
    }


    protected abstract List<T> getValues();


    @Override
    public abstract T getItem(int i);



    /**
     * Указать неперетаскиваемый хэдер
     */
    public void addNoDraggableHeaderPosition(int position){
        noDraggableHeaders.add(position);
    }

    /**
     * Является ли позиция перетаскиваемой
     */
    public boolean isPositionDraggable(int position){
        return !noDraggableHeaders.contains(position);
    }

    /**
     * Очистить список неперетаскиваемых хэдеров
     */
    public void cleanNoDraggableHeaders(){
        noDraggableHeaders.clear();
    }


    /**
     * Можно ли перетаскивать итемы в гриде
     */
    public boolean isAllowDragGrid() {
        return allowDragGrid;
    }

    /**
     * Установить возможность перетаскивать итемы в гриде
     */
    public void setAllowDragGrid(boolean allow) {
        if (allowDragGrid != allow) {
            allowDragGrid = allow;
            notifyDataSetChanged();
        }
        else
            allowDragGrid = allow;
    }


    /**
     * setFooterClickListener
     */
    public void setFooterClickListener(FooterClickListener footerClickListener) {
        this.footerClickListener = footerClickListener;
    }


    /**
     * setHeaderClickListener
     */
    public void setHeaderClickListener(HeaderClickListener headerClickListener) {
        this.headerClickListener = headerClickListener;
    }


    /**
     * setOnDragListener
     */
    public void setOnDragListener(DraggableGridView.OnDragListener onDragListener) {
        this.onDragListener = onDragListener;
    }

    /**
     * setOnDropListener
     */
    public void setOnDropListener(DraggableGridView.OnDropListener onDropListener) {
        this.onDropListener = onDropListener;
    }

    /**
     * setOnGridItemClickListener
     */
    public void setOnGridItemClickListener(OnGridItemClickListener onGridItemClickListener) {
        this.onGridItemClickListener = onGridItemClickListener;
    }

    /**
     * Включить или отключить анимацаю дрожания элементов листа в режиме редактирования
     */
    public void setListWobbleAnimation(boolean listWobbleAnimation) {
        this.listWobbleAnimation = listWobbleAnimation;
    }


    /**
     * Сейчас режим перетаскивания листа?
     */
    public boolean isGridDragNow() {
        return isGridDragNow;
    }


    /**
     * Свернут ли сейчас лист до хэдеров
     */
    public boolean isAllCollapsed() {
        return isAllCollapsed;
    }


    protected void updateDraggableGridView(DraggableGridView gridView) {

    }

    /**
     * Свернуть лист до хэдеров
     */
    public void collapseAll() {
        int first = draggableListView.getFirstVisiblePosition();
        isAllCollapsed = true;
        for (ViewHolderBase viewHolder : holdersSet) {
            viewHolder.collapse();
            viewHolder.header.setOnLongClickListener(null);
            viewHolder.header.setOnClickListener(null);

            if (listWobbleAnimation) {
                animateWobble(viewHolder, viewHolder.position % 2 == 0);
            }
        }
        draggableListView.setSelection(first);
    }


    /**
     * Развернуть все элементы листа (хэдеры)
     */
    public void expandAll() {
        isAllCollapsed = false;
        for (ViewHolderBase viewHolder : holdersSet) {
            viewHolder.expand();
            if (!noDraggableHeaders.contains(viewHolder.position)){
                viewHolder.header.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        draggableListView.setDragMode(true);
                        collapseAll();
                        return false;
                    }
                });
            }
            if (listWobbleAnimation)
                stopWobble(viewHolder);
        }
        holdersSet.clear();
        notifyDataSetChanged();
    }


    /**
     * Отменить режим перетаскивания грида(-ов)
     */
    public void cancelAllEditModes() {
        Lo.i("cancelAllEditModes... ");
        for (DraggableGridView draggableGridView : gridsInEditMode) {
            draggableGridView.stopEditMode();
        }
        gridsInEditMode.clear();
        isGridDragNow = false;
    }


    /**
     * Служебный метод - взять верхнюю и нижнюю позицию перетаскиваемого View
     */
    public int[] getDraggableGridYY() {
        return draggableGridYY;
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolderBase viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            viewHolder = new ViewHolderBase();
            viewHolder.homeLay = (ViewGroup) convertView.findViewById(R.id.lay_base);
            viewHolder.collapsableView = (ViewGroup) convertView.findViewById(R.id.lay_after_header);
            viewHolder.header = (ViewGroup) convertView.findViewById(R.id.lay_header);
            viewHolder.footer = (ViewGroup) convertView.findViewById(R.id.lay_footer);
            viewHolder.draggableGridView = (DraggableGridView) convertView.findViewById(R.id.dynamic_gridview);

            updateDraggableGridView(viewHolder.draggableGridView);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderBase) convertView.getTag();
        }
        viewHolder.position = position;


        DraggableGridAdapter adapter = new DraggableGridAdapter(context, getItem(position).getGridItems(), columnsCnt) {
            @Override
            public View getView(int positionGrid, View convertView, ViewGroup parent) {
                return getViewForGrid(position, positionGrid, convertView, parent);
            }
        };

        viewHolder.draggableGridView.setEditModeEnabled(allowDragGrid);

        viewHolder.draggableGridView.setAdapter(adapter);

        if (gridItemHeight == 0) {
            // расчитываем на постоянную высоту итема грида иначе надо делать более затратные вычисления adapter.getTotalHeight
            gridItemHeight = adapter.getItemHeight(viewHolder.draggableGridView);
            if (Build.VERSION.SDK_INT>=16)
                gridItemHeightSpacing = viewHolder.draggableGridView.getVerticalSpacing();
        }

        int tmp = getItem(position).getGridItems().size() % columnsCnt;
        int rows = (getItem(position).getGridItems().size() / columnsCnt + (tmp == 0 ? 0 : 1));
        final int totalHeight = gridItemHeight * rows + (gridItemHeightSpacing*(rows-1));
        ViewGroup.LayoutParams params = viewHolder.draggableGridView.getLayoutParams();
        params.height = totalHeight;
        viewHolder.draggableGridView.setLayoutParams(params);
        viewHolder.draggableGridView.requestLayout();


        // после пересоздания вида edit mode спадает сам
        if (gridsInEditMode.contains(viewHolder.draggableGridView)) gridsInEditMode.remove(viewHolder.draggableGridView);


        if (allowDragGrid)
        viewHolder.draggableGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (!allowDragGrid) return true;
                cancelAllEditModes();
                viewHolder.draggableGridView.startEditMode(position);
                gridsInEditMode.add(viewHolder.draggableGridView);
                isGridDragNow = true;

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
                    draggableGridYY[0] = (int) (viewHolder.homeLay.getY() + viewHolder.collapsableView.getY());
                } else {
                    int[] arr1 = new int[2];
                    viewHolder.homeLay.getLocationOnScreen(arr1);
                    draggableGridYY[0] = (int) (arr1[1]);
                    Lo.i("arr1[1] " + arr1[1]);
                }
                draggableGridYY[1] = draggableGridYY[0] + totalHeight;

                return true;
            }
        });

        viewHolder.draggableGridView.setOnItemClickListener(this);
        viewHolder.draggableGridView.setOnDragListener(this);
        viewHolder.draggableGridView.setOnDropListener(this);


        holdersSet.add(viewHolder);

        if (isAllCollapsed && !viewHolder.isCollapsed) viewHolder.collapse();

        if (!isAllCollapsed && (viewHolder.isCollapsed || viewHolder.collapsableView.getVisibility() != View.VISIBLE)) viewHolder.expand();

        if (listWobbleAnimation) {
            if (isAllCollapsed) {
                animateWobble(viewHolder, position % 2 == 0);
            } else
                stopWobble(viewHolder);
        }


        if (needHeader) {
                View lastHeaderView = viewHolder.header.getChildAt(0);
                View headerView = initHeaderView(lastHeaderView, viewHolder.header, position);
                if (lastHeaderView == null && headerView != null)
                    viewHolder.header.addView(headerView);
                viewHolder.header.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (headerClickListener != null) headerClickListener.onClick(position);
                    }
                });
        } else
            viewHolder.header.setVisibility(View.GONE);

        if (needFooter) {
            if (getItem(position).getGridItems().size()>0) {
                if (viewHolder.footer.getVisibility()!= View.VISIBLE) viewHolder.footer.setVisibility(View.VISIBLE);
                View lastFooterView = viewHolder.footer.getChildAt(0);
                View footerView = initFooterView(lastFooterView, viewHolder.footer, position);
                if (lastFooterView == null && footerView != null) viewHolder.footer.addView(footerView);
                viewHolder.footer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (footerClickListener != null) footerClickListener.onClick(position);
                    }
                });
            } else
                if (viewHolder.footer.getVisibility()!= View.GONE) viewHolder.footer.setVisibility(View.GONE);
        } else
            viewHolder.footer.setVisibility(View.GONE);


        if (!isAllCollapsed && !noDraggableHeaders.contains(position))
            viewHolder.header.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    cancelAllEditModes();

                    draggableListView.setDragMode(true);
                    collapseAll();
                    return false;
                }
            });


        return convertView;
    }


    /**
     * Инициализация хэдера - необходимо переопределять, по аналогии с простым адаптером
     */
    public View initHeaderView(View headerView, ViewGroup homeHeader, int position) {

        return headerView;
    }

    /**
     * Инициализация футера - необходимо переопределять, по аналогии с простым адаптером
     */
    public View initFooterView(View footerView, ViewGroup homeFooter, int position) {

        return footerView;
    }

    /**
     * Инициализация элемента грида - необходимо переопределять, по аналогии с простым адаптером
     */
    public View getViewForGrid(int positionList, int positionGrid, View convertView, ViewGroup parent) {

        return convertView;
    }


    /** Grid listener */
    @Override
    public void onDragStarted(int position){
        if (onDragListener!=null) onDragListener.onDragStarted(position);
    };

    /** Grid listener */
    @Override
    public void onDragPositionsChanged(int oldPosition, int newPosition){
        if (onDragListener!=null) onDragListener.onDragPositionsChanged(oldPosition, newPosition);
    };

    /** Grid listener */
    @Override
    public void onActionDrop(){
        if (onDropListener!=null) onDropListener.onActionDrop();
        cancelAllEditModes();
    }

    /** Grid Item Click Listener */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(onGridItemClickListener != null) {
            onGridItemClickListener.onGridItemClick((DraggableListItem.DraggableGridItem) parent.getAdapter().getItem(position));
        }
    }

    /** List listener */
    @Override
    public void onItemDrag(DraggableListView parent, View view, int position, long id) {
    //    Lo.i("onItemDrag " + position);
    }

    /** List listener */
    @Override
    public void onItemDrop(final DraggableListView parent, View view, int startPosition, final int endPosition, long id) {
    //    Lo.i("onItemDrop " + startPosition + "  -> " + endPosition);
        //TODO для перескока через игнорируемые нужно доделать
        if (endPosition>=0 && !noDraggableHeaders.contains(endPosition)) {
            T startItem = getValues().get(startPosition);

            if (startPosition < endPosition) {
                for (int i = startPosition; i < endPosition; ++i) {
                    getValues().set(i, getValues().get(i + 1));
                }

            } else if (endPosition < startPosition) {
                for (int i = startPosition; i > endPosition; --i) {
                    getValues().set(i, getValues().get(i - 1));
                }
            }

            getValues().set(endPosition, startItem);
        }

        draggableListView.setDragMode(false);
        parent.postDelayed(new Runnable() {
            @Override
            public void run() {
                expandAll();
                draggableListView.setSelection(endPosition);
            }
        }, 300);
    }


    //TODO
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void stopWobble(ViewHolderBase viewHolderBase) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            if (wobbleAnimators.size() != 0) {
                for (Animator wobbleAnimator : wobbleAnimators) {
                    wobbleAnimator.cancel();
                }
                wobbleAnimators.clear();
            }
            if (viewHolderBase.isAnimated) {
                viewHolderBase.header.setRotation(0);
                viewHolderBase.isAnimated = false;
            }
        } else {
            if (wobbleAnimatorsPreHoneycomb.size() != 0) {
                for (com.nineoldandroids.animation.Animator wobbleAnimator : wobbleAnimatorsPreHoneycomb) {
                    wobbleAnimator.cancel();
                }
                wobbleAnimatorsPreHoneycomb.clear();
            }
            if (viewHolderBase.isAnimated) {
                ViewHelper.setRotation(viewHolderBase.header, 0);
                viewHolderBase.isAnimated = false;
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void animateWobble(ViewHolderBase viewHolderBase, boolean inverse) {
        if (viewHolderBase.isAnimated) return;
        viewHolderBase.isAnimated = true;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            ObjectAnimator animator = createBaseWobble(viewHolderBase.header);
            if (inverse)
                animator.setFloatValues(.5f, -.5f);
            else
                animator.setFloatValues(-.5f, .5f);
            animator.start();
            wobbleAnimators.add(animator);
        } else {
            com.nineoldandroids.animation.ObjectAnimator animator = createBaseWobblePreHoneycomb(viewHolderBase.header);
            if (inverse)
                animator.setFloatValues(.5f, -.5f);
            else
                animator.setFloatValues(-.5f, .5f);
            animator.start();
            wobbleAnimatorsPreHoneycomb.add(animator);
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private ObjectAnimator createBaseWobble(View v) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) return null;
        ObjectAnimator animator = new ObjectAnimator();
        animator.setDuration(180);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setPropertyName("rotation");
        animator.setTarget(v);
        return animator;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private com.nineoldandroids.animation.ObjectAnimator createBaseWobblePreHoneycomb(View v) {
        com.nineoldandroids.animation.ObjectAnimator animator = new com.nineoldandroids.animation.ObjectAnimator();
        animator.setDuration(180);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setPropertyName("rotation");
        animator.setTarget(v);
        return animator;
    }


    public interface OnGridItemClickListener {
        public void onGridItemClick(DraggableListItem.DraggableGridItem item);
    }

    private class ViewHolderBase {

        public ViewGroup homeLay;
        public ViewGroup header, collapsableView, footer;
        public DraggableGridView draggableGridView;
        public boolean isCollapsed;
        public boolean isAnimated;
        public int position;

        public void collapse() {
            if (collapsableView != null) {
                ExpanderCollapser.collapse(collapsableView, 1);
                isCollapsed = true;
            }
        }

        public void expand() {
            if (collapsableView != null) {
                ExpanderCollapser.expand(collapsableView, 0);
                isCollapsed = false;
            }
        }
    }
}
