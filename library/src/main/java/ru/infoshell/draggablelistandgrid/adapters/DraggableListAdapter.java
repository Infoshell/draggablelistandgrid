package ru.infoshell.draggablelistandgrid.adapters;

import android.content.Context;

import java.util.List;

import ru.infoshell.draggablelistandgrid.DraggableListItem;
import ru.infoshell.draggablelistandgrid.DraggableListView;

/**
 * Адаптер для листа с гридами, с возможностью перетаскиваний.
 * <br> Для использования необходимо наследоваться от этого класса и переопределить методы:
 * <br> - initHeaderView(...)
 * <br> - initFooterView(...)
 * <br> - getViewForGrid(...)
 * <br> В наследуемом классе можно следовать паттерну ViewHolder для оптимизации прокрутки
 * <p/>
 * <br> Внимание! Т.к. внутри элемента листа грид развернут на всю высоту, в этом варианте реализации
 * возможны притормаживания на промотке при большом числе элементов грида
 * <p/>
 * <br><br>  Created by Andrey Riyk
 */
public abstract class DraggableListAdapter extends BaseDraggableListAdapter {

    protected List<DraggableListItem> values;

    public DraggableListAdapter(Context context, List<DraggableListItem> values, DraggableListView draggableListView, int columnsCnt, boolean needHeader, boolean needFooter) {
        super(context, draggableListView, columnsCnt, needHeader, needFooter);

        this.values = values;
    }

    @Override
    protected List<DraggableListItem> getValues() {
        return values;
    }

    @Override
    public int getCount() {
        return values.size();
    }


    @Override
    public DraggableListItem getItem(int position) {
        return values.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }
}
