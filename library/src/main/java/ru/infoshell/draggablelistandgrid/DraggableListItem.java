package ru.infoshell.draggablelistandgrid;

import java.util.List;

/**
 * Базовый интерфейс структуры данных для адаптера
 * <br> DraggableListItem - элемент List-а, в него иерархически входят листы DraggableGridItem
 *
 * <br> <br> Created by Andrey Riyk
 */
public interface DraggableListItem {

    /** Возвращает изначальный порядковый номер, который не должен меняться после перетаскивания */
    public int getId();

    /** Лист элементов грида внутри одной группы */
    public List<? extends DraggableGridItem> getGridItems();


    /** DraggableGridItem - один элемент Grid-а */
    public static interface DraggableGridItem {

        /** Возвращает изначальный порядковый номер, который не должен меняться после перетаскивания */
        public int getId();

    }
}
