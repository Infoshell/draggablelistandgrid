package ru.infoshell.draggablelistandgrid.lib2;

/**
 * Created by Andrey Riyk
 */
public interface RecyclerDraggableItem {

    public boolean isHeader();

    public int getHeaderId();

    public int getId2();

}
