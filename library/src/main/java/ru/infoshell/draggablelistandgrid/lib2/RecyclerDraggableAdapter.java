package ru.infoshell.draggablelistandgrid.lib2;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.infoshell.draggablelistandgrid.DraggableListItem;
import ru.infoshell.draggablelistandgrid.R;

public class RecyclerDraggableAdapter <T extends RecyclerDraggableItem> extends RecyclerView.Adapter<RecyclerDraggableAdapter.ViewHolder> {

    protected List<T> values;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;

        public ViewHolder( View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.tv_in_footer_example);
        }
    }



    public RecyclerDraggableAdapter(List<T> values) {
        this.values = values;
    }



    @Override
    public RecyclerDraggableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_example, parent, false);
        // set the view's size, margins, paddings and layout parameters
        //    ...
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        RecyclerDraggableItem item = values.get(position);
        if (item.isHeader()) {
            holder.mTextView.setText("Group: "+item.getHeaderId() );
            holder.mTextView.setBackgroundColor(Color.RED);
         //   holder.mTextView.setHeight(60);
        }
        else {
            holder.mTextView.setText("Group: "+item.getHeaderId()+ "    \n\nitem: "+item.getId2() );
            holder.mTextView.setBackgroundColor(Color.parseColor("#55555555"));
        //    holder.mTextView.setHeight(180);
        }
    }



    @Override
    public int getItemCount() {
        return values.size();
    }
}
