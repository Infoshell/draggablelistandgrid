package ru.infoshell.draggablelistandgrid;

/**
 * Created by Andrey Riyk
 */
public interface FooterClickListener {

    public void onClick(int position);

}
