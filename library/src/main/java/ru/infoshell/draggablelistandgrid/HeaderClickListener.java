package ru.infoshell.draggablelistandgrid;

/**
 * Created by Andrey Riyk
 */
public interface HeaderClickListener {

    public void onClick(int position);

}
