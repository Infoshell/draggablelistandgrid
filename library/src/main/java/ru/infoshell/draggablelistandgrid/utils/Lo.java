package ru.infoshell.draggablelistandgrid.utils;

import android.util.Log;


/**
* Утилитный класс для логгирования. Отключение логов - NEED_LOG
*
* <br> <br> Created by Andrey Riyk
*/
public class Lo {

    private static final String LOG = "draggablelistandgrid";

    private static final boolean NEED_LOG = true;



	public static void d(String s, String s2){Lo.d(s2); }
	public static void d(String s){
		if (NEED_LOG )
			Log.d(LOG, s);
	}

	public static void v(String s, String s2){Lo.v(s2); }
	public static void v(String s){
		if (NEED_LOG )
			Log.v(LOG, s);
	}

	public static void i(String s, String s2){Lo.i(s2); }
	public static void i(String s){
		if (NEED_LOG )
			Log.i(LOG, s);
	}

	public static void e(String s, String s2){Lo.e(s2); }
	public static void e(String s){
		if (NEED_LOG )
			Log.e(LOG, s);
	}

	public static void w(String s, String s2){Lo.w(s2); }
	public static void w(String s){
		if (NEED_LOG )
			Log.w(LOG, s);
	}


}
