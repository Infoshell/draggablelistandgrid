package ru.infoshell.draggablelistandgrid.utils;


/**
 * Утилитный класс для измерения задержек
 * <br> Пример использования:
 * <br> TimerMeter tm = new TimerMeter("test");
 * <br> ...
 * <br> tm.end();
 *
 * <br> <br> Created by Andrey Riyk
 */

@SuppressWarnings("ALL")
public class TimerMeter {
	
	public static final String LOG = "TIMER: ";
	
	private long start;
	private String info;
	
	public TimerMeter(String info){
		start = System.currentTimeMillis();
		this.info = info;
	}
	
	public long end(){
		long totalTime = System.currentTimeMillis() - start;
		Lo.w( LOG + info + "  time: " + totalTime);
		return totalTime;
	}

    public long end(String extraInfo){
        long totalTime = System.currentTimeMillis() - start;
        Lo.w( LOG + info+ "( "+extraInfo + ")  time: " + totalTime);
        return totalTime;
    }

}
