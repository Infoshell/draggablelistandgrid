package ru.infoshell.draggablelistandgrid.grid;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;

import java.util.HashMap;
import java.util.List;

import ru.infoshell.draggablelistandgrid.DraggableListItem;
import ru.infoshell.draggablelistandgrid.utils.Lo;


public abstract class DraggableGridAdapter extends BaseAdapter {

    private Context context;
    public static final int INVALID_ID = -1;
    private int nextStableId = 0;
    private HashMap<Object, Integer> idMap = new HashMap<Object, Integer>();
    private List<? extends DraggableListItem.DraggableGridItem> values;
    private int columnCount;


    public DraggableGridAdapter(Context context, List<? extends DraggableListItem.DraggableGridItem> values, int columnCount) {
        this.context = context;
        this.columnCount = columnCount;
        init(values);
    }


    private void init(List<? extends DraggableListItem.DraggableGridItem> values) {
        this.values = values;
        addAllStableId(values);
    }




    @Override
    public int getCount() {
        return values.size();
    }


    @Override
    public DraggableListItem.DraggableGridItem getItem(int position) {
        return values.get(position);
    }


    public int getColumnCount() {
        return columnCount;
    }


    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
        notifyDataSetChanged();
    }


    public void reorderItems(int originalPosition, int newPosition) {
        if (newPosition < getCount()) {
            reorder(values, originalPosition, newPosition);
            notifyDataSetChanged();
        }
    }







    @Override
    public final boolean hasStableIds() {
        return true;
    }


    protected void addStableId(Object item) {
        idMap.put(item, nextStableId++);
    }


    protected void addAllStableId(List<?> items) {
        for (Object item : items) {
            addStableId(item);
        }
    }


    @Override
    public final long getItemId(int position) {
        if (position < 0 || position >= idMap.size()) {
            return INVALID_ID;
        }
        Object item = getItem(position);
        return idMap.get(item);
    }


    protected void clearStableIdMap() {
        idMap.clear();
    }


    protected void removeStableID(Object item) {
        idMap.remove(item);
    }






    public List<? extends DraggableListItem.DraggableGridItem> getItems() {
        return values;
    }

    protected Context getContext() {
        return context;
    }



    public static void reorder(List list, int indexFrom, int indexTwo) {
        Lo.i("reorder " + indexFrom + " -> " + indexTwo);
        Object obj = list.remove(indexFrom);
        list.add(indexTwo, obj);
    }

    public static void swap(List list, int firstIndex, int secondIndex) {
        Object firstObject = list.get(firstIndex);
        Object secondObject = list.get(secondIndex);
        list.set(firstIndex, secondObject);
        list.set(secondIndex, firstObject);
    }

    public static float getViewX(View view) {
        return Math.abs((view.getRight() - view.getLeft()) / 2);
    }

    public static float getViewY(View view) {
        return Math.abs((view.getBottom() - view.getTop()) / 2);
    }






    /** Взять точную высоту грида (долгий метод) */
    public int getTotalHeight(DraggableGridView gridView){

        int totalHeight = 0;

        for (int i = 0; i < getCount(); i+=getColumnCount()) {
            View mView = getView(i, null, gridView);

            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight();

        }

        return totalHeight;
    }



    /** Взять высоту одной ячейки */
    public int getItemHeight(DraggableGridView gridView) {

        View mView = getView(0, null, gridView);

        mView.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        int h = mView.getMeasuredHeight();

        return h;
    }
}
