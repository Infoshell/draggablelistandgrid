package ru.infoshell.draggablelistandgrid.sample;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.infoshell.draggablelistandgrid.DraggableListView;
import ru.infoshell.draggablelistandgrid.adapters.DraggableListAdapter;
import ru.infoshell.draggablelistandgrid.DraggableListItem;
import ru.infoshell.draggablelistandgrid.FooterClickListener;
import ru.infoshell.draggablelistandgrid.HeaderClickListener;
import ru.infoshell.draggablelistandgrid.grid.DraggableGridView;
import ru.infoshell.draggablelistandgrid.lib2.DragLayoutManager;
import ru.infoshell.draggablelistandgrid.lib2.FixedGridLayoutManager;
import ru.infoshell.draggablelistandgrid.lib2.RecyclerDraggableAdapter;
import ru.infoshell.draggablelistandgrid.sample.lib2.MyRecyclerDraggableItem;
import ru.infoshell.draggablelistandgrid.utils.Lo;

/**
 * Тестовая Activity для примера работы {@link DraggableListAdapter}
 *
 * <br>Для использования библиотеки необходимо:
 * <br> 1. Унаследоваться от DraggableListAdapter и переопределить 3 метода
 * <br> 2. В свою структуру данных внедрить 2 интерфейса: DraggableListItem и DraggableListItem.DraggableGridItem
 * <br> 3. Использовать всё это как обычный адаптер с листом
 *
 * <br> <br> Created by Andrey Riyk
 */

public class MainActivity extends Activity {

    private static final boolean LIB1 = false;

    private DraggableListAdapter adapter;
    private DraggableListView list;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (LIB1) {
            setContentView(R.layout.activity_main);

            // тестовый набор данных для адаптера
            final List<DraggableListItem> values = getTestDraggableListItem(100);

            // инициализация DraggableListAdapter и DraggableListView
            list = (DraggableListView) findViewById(R.id.list);
            adapter = new MyDraggableListAdapter(getApplicationContext(), values, list, 3, true, true);
            list.setDragAdapter(adapter);
            list.setDraggingEnabled(true);
            list.setDividerHeight(0);

            adapter.setFooterClickListener(new FooterClickListener() {
                @Override
                public void onClick(int position) {
                    Toast.makeText(getApplicationContext(), "FooterClickListener " + values.get(position).getId(), Toast.LENGTH_SHORT).show();
                }
            });
            adapter.setHeaderClickListener(new HeaderClickListener() {
                @Override
                public void onClick(int position) {
                    Toast.makeText(getApplicationContext(), "HeaderClickListener " + values.get(position).getId(), Toast.LENGTH_SHORT).show();
                }
            });


            adapter.setOnDropListener(new DraggableGridView.OnDropListener() {
                @Override
                public void onActionDrop() {
                    Lo.w("Yo-ho-ho, this is setOnDropListener onActionDrop()");
                }
            });

            adapter.setOnDragListener(new DraggableGridView.OnDragListener() {
                @Override
                public void onDragStarted(int position) {
                    Lo.w("Yo-ho-ho, this is setOnDragListener onDragStarted() " + position);
                }

                @Override
                public void onDragPositionsChanged(int oldPosition, int newPosition) {
                    Lo.w("Yo-ho-ho, this is setOnDragListener onDragPositionsChanged() " + oldPosition + " -> " + newPosition);
                }
            });

            //    adapter.setAllowDragGrid(false);

            adapter.addNoDraggableHeaderPosition(0);
        }


        else{
            setContentView(R.layout.activity_main2);

            // тестовый набор данных для адаптера
            List<MyRecyclerDraggableItem> values = getTestMyRecyclerDraggableItem(500);

            RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView.setHasFixedSize(false);

            // use a linear layout manager
            RecyclerDraggableAdapter recyclerDraggableAdapter = new RecyclerDraggableAdapter<MyRecyclerDraggableItem>(values);
            RecyclerView.LayoutManager mLayoutManager = new DragLayoutManager(recyclerDraggableAdapter);
            mRecyclerView.setLayoutManager(mLayoutManager);

            mRecyclerView.setAdapter(recyclerDraggableAdapter);

        }
    }



    private List<DraggableListItem> getTestDraggableListItem(int size1){
        final List<DraggableListItem> values = new ArrayList<DraggableListItem>();
        // наполняем лист хэдеров
        for (int i = 0; i < size1; i++) {

            // наполняем элементы внутри хэдера
            final List<DraggableListItem.DraggableGridItem> gridItems = new ArrayList<DraggableListItem.DraggableGridItem>();
            for (int i2 = 0; i2 < (i == 10 ? 10 : (i % 9)); i2++) {
                gridItems.add(new MyTestGridItem(i2));
            }

            MyTestListItem item = new MyTestListItem(i, gridItems);
            values.add(item);
        }
        return values;
    }



    private List<MyRecyclerDraggableItem> getTestMyRecyclerDraggableItem(int size){
        final List<MyRecyclerDraggableItem> values = new ArrayList<MyRecyclerDraggableItem>();
        final int groupSize = 10;
        for (int i = 0; i < size; i++) {
            MyRecyclerDraggableItem item = new MyRecyclerDraggableItem(i%groupSize==0, i/groupSize, i%groupSize);
            values.add(item);
        }
        return values;
    }



    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (LIB1 && keyCode == KeyEvent.KEYCODE_BACK) {
            if (adapter.isAllCollapsed()){
                adapter.expandAll();
                list.setDragMode(false);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
