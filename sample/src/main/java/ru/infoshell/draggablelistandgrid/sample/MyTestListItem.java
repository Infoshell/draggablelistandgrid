package ru.infoshell.draggablelistandgrid.sample;

import java.util.List;

import ru.infoshell.draggablelistandgrid.DraggableListItem;

/**
 * Тестовый элемент листа
 * <p/>
 * <br><br> Created by Andrey Riyk
 */
public class MyTestListItem implements DraggableListItem {

    private int id;
    private List<? extends DraggableListItem.DraggableGridItem> gridItems;

    public MyTestListItem(int position, List<? extends DraggableListItem.DraggableGridItem> list) {
        gridItems = list;
        id = position;
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public List<? extends DraggableListItem.DraggableGridItem> getGridItems() {
        return gridItems;
    }

}
