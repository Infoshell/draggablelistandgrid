package ru.infoshell.draggablelistandgrid.sample;

import ru.infoshell.draggablelistandgrid.DraggableListItem;

/**
 * Тестовый элемент грида
 *
 * <br><br> Created by Andrey Riyk
 */
public class MyTestGridItem implements DraggableListItem.DraggableGridItem {

    private int id;

    public MyTestGridItem(int positionInGrid){
        id = positionInGrid;
    }


    // пример метода, который берет изображения для элемента грида
    public int getImageResId(){
        return id%3==0? R.drawable.pic1 : id%3==1? R.drawable.pic2 : R.drawable.pic3;
    }



    @Override
    public int getId() {
        return id;
    }
}
