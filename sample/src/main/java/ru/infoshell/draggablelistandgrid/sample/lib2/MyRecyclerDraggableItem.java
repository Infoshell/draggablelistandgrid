package ru.infoshell.draggablelistandgrid.sample.lib2;

import ru.infoshell.draggablelistandgrid.lib2.RecyclerDraggableItem;

/**
 * Created by Andrey Riyk
 */
public class MyRecyclerDraggableItem implements RecyclerDraggableItem {

    private boolean isHeader;
    private int groupNum, totalNum;

    public MyRecyclerDraggableItem(boolean isHeader, int groupNum, int totalNum) {
        this.isHeader = isHeader;
        this.groupNum = groupNum;
        this.totalNum = totalNum;
    }

    public boolean isHeader(){
        return isHeader;
    }

    @Override
    public int getHeaderId() {
        return groupNum;
    }

    @Override
    public int getId2() {
        return totalNum;
    }

}
