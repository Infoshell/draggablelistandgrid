package ru.infoshell.draggablelistandgrid.sample;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.infoshell.draggablelistandgrid.adapters.DraggableListAdapter;
import ru.infoshell.draggablelistandgrid.DraggableListItem;
import ru.infoshell.draggablelistandgrid.DraggableListView;

/**
 * <br>Тестовый адаптер, см. {@link DraggableListAdapter}
 *
 * <br><br>Created by Andrey Riyk
 *
 */
public class MyDraggableListAdapter extends DraggableListAdapter {

    private final LayoutInflater inflater;


    public MyDraggableListAdapter(Context context, List<DraggableListItem> values, DraggableListView draggableListView, int columnsCnt,
                                  boolean needHeader, boolean needFooter) {
        super(context, values, draggableListView, columnsCnt, needHeader, needFooter);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public View initHeaderView(View headerView, ViewGroup homeHeader, int position){
        final ViewHolder viewHolder;
        if(headerView==null){
            headerView = inflater.inflate(R.layout.header_lay_example, homeHeader, false);
            viewHolder = new ViewHolder();
            viewHolder.tvInHeader = (TextView) headerView.findViewById(R.id.tv_in_example_header);
            headerView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) headerView.getTag();
        }
        viewHolder.tvInHeader.setText("Header "+values.get(position).getId() );
        return headerView;
    }



    @Override
    public View initFooterView(View footerView, ViewGroup homeFooter, int position){
        final ViewHolder viewHolder;
        if(footerView==null){
            footerView = inflater.inflate(R.layout.footer_lay_example, homeFooter, false);
            viewHolder = new ViewHolder();
            viewHolder.tvInFooter = (TextView) footerView.findViewById(R.id.tv_in_footer_example);
            footerView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) footerView.getTag();
        }
        viewHolder.tvInFooter.setTextColor(Color.GRAY);
        viewHolder.tvInFooter.setText("Footer "+values.get(position).getId() );

        return footerView;
    }



    class ViewHolder{
        TextView tvInHeader, tvInFooter;
    }



    @Override
    public View getViewForGrid(int positionList, int positionGrid, View convertView, ViewGroup parent) {
        GridViewHolder gridViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_item_example, null);
            gridViewHolder = new GridViewHolder();
            gridViewHolder.textView = (TextView) convertView.findViewById(R.id.item_title);
            gridViewHolder.image = (ImageView) convertView.findViewById(R.id.item_img);
            convertView.setTag(gridViewHolder);
        } else {
            gridViewHolder = (GridViewHolder) convertView.getTag();
        }

        if (values.get(positionList).getGridItems().size()>0) {
            gridViewHolder.textView.setText("item " + values.get(positionList).getGridItems().get(positionGrid).getId());

            gridViewHolder.image.setImageResource(
                    ((MyTestGridItem) (values.get(positionList).getGridItems().get(positionGrid))).getImageResId()
            );
        }

        return convertView;
    }



    class GridViewHolder{
        private TextView textView;
        private ImageView image;
    }
}
